package com.rdevelops.userdefinedcalener

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {

    private val dayTextViewList = arrayListOf<Int>()
    private val weekList = arrayListOf<String>()
    private val monthList = arrayListOf<String>()
    private val totalDayOfMonthsList = arrayListOf<Int>()
    private val yearList = arrayListOf<Int>()
    private var selectedValues = arrayListOf<Int>()
    private val holidayList = arrayListOf<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeArrays()

        val monthSpinner = findViewById<Spinner>(R.id.monthSpinner)
        val yearSpinner = findViewById<Spinner>(R.id.yearSpinner)
        val selectDateSpinner = findViewById<Spinner>(R.id.selectDateSpinner)

        if (monthSpinner != null) {
            val monthAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, monthList)
            monthSpinner.adapter = monthAdapter
        }

        if (yearSpinner != null) {
            val yearAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, yearList)
            yearSpinner.adapter = yearAdapter
        }

        if (selectDateSpinner != null) {
            val selectDateAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, weekList)
            selectDateSpinner.adapter = selectDateAdapter
        }

        monthSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                selectedValues[0] = position
                println("month position "+selectedValues)
                if (selectedValues[0] != 1000 &&
                        selectedValues[1] != 1000 &&
                        selectedValues[2] != 1000) {
                    resetCalender()
                    setCalender(selectedValues)
                }
                val selectedMonth = findViewById<TextView>(R.id.setMonthTextView)
                selectedMonth.text = monthList[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        yearSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                selectedValues[1] = position
                println("month position "+selectedValues)
                if (selectedValues[0] != 1000 &&
                        selectedValues[1] != 1000 &&
                        selectedValues[2] != 1000) {
                    resetCalender()
                    setCalender(selectedValues)
                }
                val selectedYear = findViewById<TextView>(R.id.setYearTextView)
                selectedYear.text = yearList[position].toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        selectDateSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                selectedValues[2] = position
                println("month position "+selectedValues)
                if (selectedValues[0] != 1000 &&
                        selectedValues[1] != 1000 &&
                        selectedValues[2] != 1000) {
                    resetCalender()
                    setCalender(selectedValues)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

    }

    private fun resetCalender() {
        for (i in 1..dayTextViewList.size) {
            findViewById<TextView>(dayTextViewList[i-1]).visibility = View.INVISIBLE
            findViewById<TextView>(dayTextViewList[i-1]).setTextColor(Color.parseColor("#000000"))
        }
    }

    private fun setCalender(selectedValues: ArrayList<Int>) {
        var range = totalDayOfMonthsList[selectedValues[0]]
        if (selectedValues[0]==1) {
            if (yearList[selectedValues[1]]%4 == 0) {
                range += 1
            }
        }
        for (i in 1..range) {
            val day = findViewById<TextView>(dayTextViewList[selectedValues[2]+(i-1)])
            day.visibility = View.VISIBLE
            day.text = i.toString()
            if (holidayList.contains(i))
                day.setTextColor(Color.parseColor("#ff0000"))
        }
    }

    private fun initializeArrays() {
        selectedValues.addAll(listOf(1000,1000,1000))
        dayTextViewList.addAll(listOf(R.id.day1TextView,R.id.day2TextView,R.id.day3TextView,R.id.day4TextView,R.id.day5TextView,R.id.day6TextView,R.id.day7TextView,R.id.day8TextView,R.id.day9TextView,R.id.day10TextView,
                R.id.day11TextView,R.id.day12TextView,R.id.day13TextView,R.id.day14TextView,R.id.day15TextView,R.id.day16TextView,R.id.day17TextView,R.id.day18TextView,R.id.day19TextView,R.id.day20TextView,
                R.id.day21TextView,R.id.day22TextView,R.id.day23TextView,R.id.day24TextView,R.id.day25TextView,R.id.day26TextView,R.id.day27TextView,R.id.day28TextView,R.id.day29TextView,R.id.day30TextView,
                R.id.day31TextView,R.id.day32TextView,R.id.day33TextView,R.id.day34TextView,R.id.day35TextView,R.id.day36TextView,R.id.day37TextView,R.id.day38TextView,R.id.day39TextView,R.id.day40TextView,
                R.id.day41TextView,R.id.day42TextView))

        yearList.addAll(listOf(2014,2015,2016,2017,2018,2019,2019,2020))

        weekList.addAll(listOf("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"))

        monthList.addAll(listOf("January","February","March","April","May","June","July","August","September","October","November","December"))

        totalDayOfMonthsList.addAll(listOf(31,28,31,30,31,30,31,31,30,31,30,31))

        holidayList.addAll(listOf(1,17,15,31,13,21))
    }

}
